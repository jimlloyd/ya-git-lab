# ya-git-lab

Yet Another Git Extension for GitLab

This is a node.js package that implements a git subcommand for integration with GitLab.

## Installation

```
npm install -g ya-git-lab
```

## Usage

```
$ git lab
NAME
        git-lab - GitLab integration

SYNOPSIS
        git lab start <issueid>
        git lab bump
        git lab done <issueid>
        git lab merge-train <mr-id>
        git lab open [<issue-id> | i<issue-id> | m<mr-id>]

DESCRIPTION
        This command facilitates using Git with GitLab. The following subcommands are available:

        start
                Use the start subcommand to start working on an issue. It starts the issue in GitLab
                and creates an appropriately named branch in your local git repository.

        bump
                Use the bump subcommand to create a new branch for the current issue, in preparation for rebasing.
                The first time this is done, it appends ".v1" to the issue name. On second and subsequent bumps
                the version numbers is bumped: .v2, .v3, etc.
                The bump subcommand does not change gitlab state.

        done
                Use the done command after your branch is merged to delete all branches associated with the issue

        merge-train
                The merge-train command requires a merge request ID. It creates a branch for the provisional merge commit
                if the train succeeds up through that MR. The branch will be named with the format
                `merge-train/${mr-id}-${commit}`, where commit is an abbreviated commitish for the merge commit.

        open
                The open command will open gitlab issue or merge request page in your default web browser.
                If no argument is provided, the issue for the current branch will be opened.
                An argument of the form `ID` or `iID` will open issue #ID.
                An argument of the form `mID` will open MR !ID.
                (You can also use "#ID" and "!ID", but only with quoting, because Bash treats both of these characters specially.)

CONFIGURATION
        You must set two git configuration variables:
            gitlab.token    A Personal Access Token that you create for API access via User Settings > Access Tokens.
            gitlab.project  The Project ID of your GitLab project displayed under Project > Details.
        You may optionally set:
            gitlab.master   The long-lived branch that serves as your master branch. git-lab will checkout this
                            branch when it deletes any branches with `git lab done <issueid>`.
            gitlab.types    If true, create branches with a prefix `<type>/` if the issue has a label `type::<type>`.
                            Supported types are `feature`, `bug`, `chore`, `epic`.
                            The default is false, i.e. do not prefix with the type.
```

## Other similar packages

This package is derived from a similar package for Pivotal Tracker integration: [ya-git-pivotal](https://github.com/jimlloyd/ya-git-pivotal)


