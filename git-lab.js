#!/usr/bin/env node
'use strict';

const _ = require('lodash');
const chalk = require('chalk');
const dlog = require('debug')('lab');
const child_process = require('child_process');
const open = require('open');
const readline = require('readline');
const util = require('util');

const Promise = require('bluebird');
const request = require("request");

const Err = chalk.red.bold;

const gitlabConfig = {
  project: null,
  token: null,
  master: 'master',
  types: false
};

var gitlabIdentity;

class UsageError extends Error
{
  // UsageError is identical in funcitonality to Error, we just need to be able to distinguish the two kinds of errors
  constructor(message) {
    super(message)
  }
}

function execCommand(cmd) {
  return new Promise(function (resolve, reject) {
    dlog('execCommand started:', cmd);
    child_process.exec(cmd, function (err, stdout, stderr) {
      if (err) {
        var message = util.format('Command <%s> failed with status code:%d.\nstderr:\n%s\n', cmd, err.code, stderr);
        reject(new Error(message));
      }
      else {
        dlog('execCommand finished:', cmd);
        resolve([stdout, stderr]);
      }
    });
  });
}

function getGitLabConfig() {
  var cmd = 'git config --get-regexp "gitlab\.(token|project|master|types)"';
  return execCommand(cmd)
    .then(outAndErr => {
      var lines = outAndErr[0].split('\n');
      lines = lines.map(function (line) { return line.trim(); });

      _.forEach(lines, function (line) {
        if (line.length===0)
          return;
        var parts = line.match(/(\w+)\s+(.+)/);
        if (!parts || parts.length !== 3)
          return;
        var name = parts[1];
        var value = parts[2];
        gitlabConfig[name] = value;
      });

      dlog('gitlabConfig:', gitlabConfig);
      return gitlabConfig;
    })
    .catch(function (err) {
      throw(new UsageError('git config variables gitlab.token and gitlab.project must be defined.'));
    });
}

function apiRequest(path, options, method) {
  options = options || {};
  method = method || 'get';
  var baseUrl = 'https://gitlab.com/api/v4/projects/' + gitlabConfig.project + '/';
  var opts = _.extend({
    method: method,
    url: baseUrl + path,
    json: true,
    headers: {
      'Private-Token': gitlabConfig.token
    }
  }, options);

  dlog('apiRequest started:', opts);

  return new Promise(function (resolve, reject) {
    request(opts, function(err, httpIncoming, body) {
      const status = httpIncoming && httpIncoming.statusCode;
      dlog(`status: ${status}`);
      if (method=='get' && status == 404) {
        resolve(null);
      }
      if (status >= 400) {
        reject(new Error(`http status: ${status}`));
      }
      if (err) {
        reject(err);
      } else {
        resolve(body);
      }
    });
  });
}

function getIdentity() {
  dlog('getIdentity');
  var options = {
    url: 'https://gitlab.com/api/v4/user'
  };
  return apiRequest('/user', options, 'get')
    .then(function (result) {
      dlog('get /user request returned result:', result);
      gitlabIdentity = result;
      return result;
    });
}

function setIssueStarted(issue) {
  dlog('setIssueStarted:', issue.iid);

  const path = 'issues/' + issue.iid;
  const { assignee_ids } = issue;
  const options = {
    body: { assignee_ids: _.union(assignee_ids, [gitlabIdentity.id]) }
  };
  dlog('Put options:', options);
  return apiRequest(path, options, 'put')
    .then(result => {
      dlog('put request returned result:', result);
      return result;
    });
}

function issueType(issue) {
  const defaultType = 'type::feature';
  // Set the issue_type here. We need to inspect the labels, looking for `chore` or `bug`.
  // If one is found, set the issue_type to it. If neither is present, set the issue_type to `feature`.
  const types = _.intersection(issue.labels, ['type::bug', 'type::chore', 'type::epic', defaultType])
  const matchedType = _.isEmpty(types) ? defaultType : types[0];
  return matchedType.slice(6);
}

function makeBranchNameForIssue(issue) {
  dlog('makeBranchNameForIssue with issue:', issue);
  const branchType = issueType(issue);
  dlog('Parsed issue type as:', issueType);
  const _title = issue.title.replace(/\W+/g, '-', 'g')
  const parts = _.compact(_title.split('-'))
  const title = _.join(parts, '-')
  const prefix = gitlabConfig.types ? `${branchType}/` : '';
  const branch = `${prefix}${issue.iid}-${title}`
  return branch;
}

function createBranch(branch) {
  dlog('createBranch:', branch);
  var cmd = 'git checkout -b ' + branch;
  process.stdout.write('\n' + chalk.gray.bold(cmd) + '\n');
  return execCommand(cmd)
    .then(function (outAndErr) {
      _.forEach(outAndErr, process.stdout.write.bind(process.stdout));
      return null;
    });
}

function help() {
  var B = chalk.bold;
  var U = chalk.underline;
  var usage = [
    B('NAME'),
    '\tgit-lab - GitLab integration',
    '',
    B('SYNOPSIS'),
    '\tgit lab start <issueid>',
    '\tgit lab bump',
    '\tgit lab done <issueid>',
    '\tgit lab merge-train <mr-id>',
    '\tgit lab open [<issue-id> | i<issue-id> | m<mr-id>]',
    '',
    B('DESCRIPTION'),
    '\tThis command facilitates using Git with GitLab. The following subcommands are available:',
    '',
    B('\tstart'),
    '\t\tUse the start subcommand to start working on an issue. It starts the issue in GitLab',
    '\t\tand creates an appropriately named branch in your local git repository.',
    '',
    B('\tbump'),
    '\t\tUse the bump subcommand to create a new branch for the current issue, in preparation for rebasing.',
    '\t\tThe first time this is done, it appends ".v1" to the issue name. On second and subsequent bumps',
    '\t\tthe version numbers is bumped: .v2, .v3, etc.',
    '\t\tThe bump subcommand does not change gitlab state.',
    '',
    B('\tdone'),
    '\t\tUse the done command after your branch is merged to delete all branches associated with the issue',
    '',
    B('\tmerge-train'),
    '\t\tThe merge-train command requires a merge request ID. It creates a branch for the provisional merge commit',
    '\t\tif the train succeeds up through that MR. The branch will be named with the format',
    '\t\t`merge-train/${mr-id}-${commit}`, where commit is an abbreviated commitish for the merge commit.',
    '',
    B('\topen'),
    '\t\tThe open command will open the gitlab issue or merge request page in your default web browser.',
    '\t\tIf no argument is provided, the issue for the current branch will be opened.',
    '\t\tAn argument of the form `ID` or `iID` will open issue #ID.',
    '\t\tAn argument of the form `mID` will open MR !ID.',
    '\t\t(You can also use "#ID" and "!ID", but only with quoting, because Bash treats both of these characters specially.)',
    '',
    B('CONFIGURATION'),
    '\tYou ' + B('must') + ' set two git configuration variables:',
    '\t    gitlab.token    A Personal Access Token that you create for API access via User Settings > Access Tokens.',
    '\t    gitlab.project  The Project ID of your GitLab project displayed under Project > Details.',
    '\tYou may optionally set:',
    '\t    gitlab.master   The long-lived branch that serves as your master branch. git-lab will checkout this',
    '\t                    branch when it deletes any branches with `git lab done <issueid>`.',
    '\t    gitlab.types    If true, create branches with a prefix `<type>/` if the issue has a label `type::<type>`.',
    '\t                    Supported types are `feature`, `bug`, `chore`, `epic`.',
    '\t                    The default is false, i.e. do not prefix with the type.',
    ''
  ];

  process.stdout.write(usage.join('\n')+'\n');
}

function getIssue(issueId) {
  dlog('Issue id on command line:', issueId);
  if (_.isEmpty(issueId)) {
    throw(new UsageError('Must specify an issueid'));
  }
  if (!_.isInteger(_.toNumber(issueId))) {
    throw(new UsageError(`Specified issueId is not an integer:${issueId}`));
  }
  const path = 'issues/' + issueId;
  return apiRequest(path)
    .tap(issue => dlog('Got issue:', issue))
    .tap(issue => {
      if (_.isEmpty(issue)) {
        throw(new UsageError(`No issue found for issueId:${issueId}`));
      }
    });
}

function startIssue(issueId) {
  return Promise.resolve()
    .then(() => getIssue(issueId))
    .then(issue => setIssueStarted(issue))
    .then(issue => makeBranchNameForIssue(issue))
    .then(branch => createBranch(branch));
}

function getCurrentBranch() {
  var cmd = 'git symbolic-ref --short HEAD';
  return execCommand(cmd)
  .then(outAndErr => {
    const lines = outAndErr[0].split('\n');
    const branch = lines[0];
    dlog('getCurrentBranch: current branch is:', branch);
    return branch;
  });
}

// Branches should be formatted like this: `(${branchType}/)${issue.iid}_${title}(.v{version})`
// We now conform to the defacto standard set by GitLab, even though it is inferior to
// our prior naming convention. This means that the branch type (feature, chore, bug, epic)
// is not present in the branch name: `${issue.iid}-${title}(.v{version})`

const branchNameRegex = /^((\w+)\/)?(\d+)[-_]([^\.]+)(\.v\d+)?$/;

function parseBranch(branch) {
  const m = branchNameRegex.exec(branch);
  dlog('matchAll returned:', m);
  if (!m) {
    throw(new UsageError(`Could not parse branch name: ${branch}`));
  }
  const [_1, _2, type, id, title, version] = m;
  const result = {type, id, title, version};
  dlog(`Branch ${branch} parsed as:`, result);
  return result;
}

function getCurrentBranchIssueId() {
  return getCurrentBranch()
  .then(branch => parseBranch(branch))
  .then(({type, id, title, version}) => id);
}

function getNewBranchName() {
  return getCurrentBranch()
  .then(branch => {
    var m = /^([^\.]+)(\.v(\d+))?$/.exec(branch);
    if (!m) {
      throw(new UsageError(`Could not parse branch name: ${branch}`));
    }

    dlog('Parsed as:', m);

    if (!m[2]) {
      m[2] = 1;
    } else {
      m[2] = parseInt(m[3]) + 1;
    }

    branch = m[1] + '.v' + m[2];
    dlog('getNewBranchName: bumped branch is:', branch);

    return branch;
  });
}


function bumpBranch() {
  return getNewBranchName()
    .then(branch => createBranch(branch));
}

function parseBranchNameFromLine(line) {
  // Most of the time we just need to trim.
  // But the current branch will be reported as '* branch-name'.
  // If present, we need remove the '*'
  const s = line.trim();
  const parts = s.split(' ');
  return _.last(parts);
}

function getAllBranchVersions(id) {
  // We first obtain from git a list of all branches that happen to have the issue id anywhere within the name.
  // This will normally be exactly the list we want, but there may be extra branches the coincidentally include
  // the issue id but do not conform to the allowed naming conventions.
  const iid = id*1;
  const cmd = `git branch --list '*${iid}*'`;
  return execCommand(cmd)
    .then(outAndErr =>  outAndErr[0].split('\n'))
    .map(b => parseBranchNameFromLine(b))
    .filter(b => {
      if (_.isEmpty(b))
      {
        return false;
      }
      const parsed = parseBranch(b);
      dlog({parsed});
      return iid === parsed.id*1;
    })
    .tap(branches => dlog(`All branches for ${id}:`, branches));
}

function checkoutBranch(branch) {
  const cmd = `git checkout ${branch}`;
  return execCommand(cmd);
}

function switchToMaster() {
  dlog('switchToMaster:', gitlabConfig.master);
  const { master } = gitlabConfig;
  if (_.isEmpty(master)) {
    // This should never happen, because we default to 'master' above
    throw new Error('No master branch specified in config');
  }
  return checkoutBranch(master);
}

function testIfBranchExists(branch) {
  const cmd = `git rev-parse --verify ${branch}`;
  return execCommand(cmd)
  .then(committish => true)
  .catch(err => false);
}

function removeBranches(issueId, branches) {
  let currentBranch = null;
  return Promise.resolve()
  .then(() => {
    if (_.isEmpty(branches)) {
      throw(new UsageError(`No branches matching issue id: ${issueId}`));
    }
  })
  .then(() => getCurrentBranch())
  .then(branch => currentBranch = branch)
  .then(() => switchToMaster())
  .then(() => {
      const cmd = 'git branch -D ' + branches.join(' ');
      return execCommand(cmd);
  })
  .then(() => {
    if (!_.includes(branches, currentBranch)) {
      return checkoutBranch(currentBranch);
    }
  });
}

function doneIssue(issueId) {
  return Promise.resolve()
  .then(() => getIssue(issueId)) // just to confirm issueId is valid
  .then(() => getAllBranchVersions(issueId))
  .then(branches => removeBranches(issueId, branches));
}

function getMergeRequest(mr) {
  return apiRequest(`merge_requests/${mr}`);
}

function mergeTrain(mr) {
  return Promise.resolve()
  .then(() => getMergeRequest(mr))
  .then(properties => {
    dlog(properties);
    const { head_pipeline } = properties;
    const { sha } = head_pipeline;
    const commit = sha.slice(0,10);
    const mergeBranchName = `merge-train/${mr}-${commit}`;
    const ref = `refs/merge-requests/${mr}/merge`
    const options = {};
    return apiRequest(`repository/branches?branch=${mergeBranchName}&ref=${ref}`, options, 'post')
    .tap(result => dlog('merge-train branch create request returned result:', result))
    .then(() => execCommand('git fetch'))
    .then(() => {
      const cmd = `git checkout ${mergeBranchName}`;
      console.log(cmd);
      return execCommand(cmd)
    });
  });
}

function openIssue(id) {
  return Promise.resolve()
  .then(() => getIssue(id))
  .then(issue => {
    const { web_url } = issue;
    return open(web_url);
  });
}

function openMergeRequest(id) {
  return Promise.resolve()
  .then(() => getMergeRequest(id))
  .then(mergeRequest => {
    const { web_url } = mergeRequest;
    return open(web_url);
  });
}

function openCurrentIssue() {
  return getCurrentBranchIssueId()
  .then(id => openIssue(id));
}

function openSomething(id) {
  return Promise.resolve()
  .then(() => {
    if (_.isEmpty(id)) {
      return openCurrentIssue();
    } else if (id[0] == '#' || id[0].toLowerCase() == 'i') {
      return openIssue(id.slice(1));
    } else if (id[0] == '!' || id[0].toLowerCase() == 'm') {
      return openMergeRequest(id.slice(1));
    } else {
      // Default to issue ids.
      return openIssue(id);
    }
  });
}

function main()
{
  const [nodepath, scriptpath, command, id, ...args] = process.argv;
  dlog({nodepath, scriptpath, command, id, args});

  return Promise.resolve()
  .then(() => {
    const commands = ['start', 'bump', 'done', 'merge-train', 'open'];
    if (!_.includes(commands, command)) {
      throw(new UsageError(`Unrecognized command ${command}`));
    }
  })
  .then(() => getGitLabConfig())
  .then(() => getIdentity())
  .then(() => {
    if (command === 'start') {
      return startIssue(id);
    }
    else if (command === 'bump') {
      return bumpBranch();
    }
    else if (command === 'done') {
      return doneIssue(id);
    }
    else if (command === 'merge-train') {
      return mergeTrain(id);
    }
    else if (command === 'open') {
      return openSomething(id);
    }
  })
  .catch(err => {
      if (err instanceof UsageError)
      {
        help();
        console.error(Err(err));
      } else {
        console.error(Err(err.message));
        console.error(Err(err));
        console.error(err.stack);
      }
  });
}

return Promise.resolve()
  .then(() => main())
